//jshint ignore : start 
import React from 'react';
function CheckoutSteps(props) {
  return <div className="checkout-steps">
    <div className={props.step1 ? 'active' : ''} >Connexion</div>
    <div className={props.step2 ? 'active' : ''} >Livraison</div>
    <div className={props.step3 ? 'active' : ''} >Paiement</div>
    <div className={props.step4 ? 'active' : ''} >Passer votre commande</div>
  </div>
}

export default CheckoutSteps;